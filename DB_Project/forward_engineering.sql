-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`countries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`countries` (
  `number_id` INT NOT NULL,
  `year` INT NOT NULL,
  `ISO_code` VARCHAR(45) NOT NULL,
  `countries` VARCHAR(45) NOT NULL,
  `region` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`number_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`pf_rol`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`pf_rol` (
  `number_id` INT NOT NULL,
  `pf_rol_procedural` DOUBLE NULL DEFAULT NULL,
  `pf_rol_civil` DOUBLE NULL DEFAULT NULL,
  `pf_rol_criminal` DOUBLE NULL DEFAULT NULL,
  `pf_rol` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`number_id`),
  INDEX `fk_pf_rol_countries1_idx` (`number_id` ASC) VISIBLE,
  CONSTRAINT `fk_pf_rol_countries1`
    FOREIGN KEY (`number_id`)
    REFERENCES `mydb`.`countries` (`number_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`pf_ss`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`pf_ss` (
  `number_id` INT NOT NULL,
  `pf_ss_homicide` DOUBLE NULL DEFAULT NULL,
  `pf_ss_disappearances_disap` DOUBLE NULL DEFAULT NULL,
  `pf_ss_disappearances_violent` DOUBLE NULL DEFAULT NULL,
  `pf_ss_disappearances_organized` DOUBLE NULL DEFAULT NULL,
  `pf_ss_disappearances_fatalities` DOUBLE NULL DEFAULT NULL,
  `pf_ss_disappearances_injuries` DOUBLE NULL DEFAULT NULL,
  `pf_ss_disappearances` DOUBLE NULL DEFAULT NULL,
  `pf_ss_women_fgm` DOUBLE NULL DEFAULT NULL,
  `pf_ss_women_missing` DOUBLE NULL DEFAULT NULL,
  `pf_ss_women_inheritance_widows` DOUBLE NULL DEFAULT NULL,
  `pf_ss_women_inheritance_daughters` DOUBLE NULL DEFAULT NULL,
  `pf_ss_women_inheritance` DOUBLE NULL DEFAULT NULL,
  `pf_ss_women` DOUBLE NULL DEFAULT NULL,
  `pf_ss` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`number_id`),
  INDEX `fk_pf_ss_countries1_idx` (`number_id` ASC) VISIBLE,
  CONSTRAINT `fk_pf_ss_countries1`
    FOREIGN KEY (`number_id`)
    REFERENCES `mydb`.`countries` (`number_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`pf_movement`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`pf_movement` (
  `number_id` INT NOT NULL,
  `pf_movement_domestic` DOUBLE NULL DEFAULT NULL,
  `pf_movement_foreign` DOUBLE NULL DEFAULT NULL,
  `pf_movement_women` DOUBLE NULL DEFAULT NULL,
  `pf_movement` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`number_id`),
  INDEX `fk_pf_movement_countries1_idx` (`number_id` ASC) VISIBLE,
  CONSTRAINT `fk_pf_movement_countries1`
    FOREIGN KEY (`number_id`)
    REFERENCES `mydb`.`countries` (`number_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`pf_religion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`pf_religion` (
  `number_id` INT NOT NULL,
  `pf_religion_estop_establish` DOUBLE NULL DEFAULT NULL,
  `pf_religion_estop_operate` DOUBLE NULL DEFAULT NULL,
  `pf_religion_estop` DOUBLE NULL DEFAULT NULL,
  `pf_religion_harassment` DOUBLE NULL DEFAULT NULL,
  `pf_religion_restrictions` DOUBLE NULL DEFAULT NULL,
  `pf_religion` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`number_id`),
  INDEX `fk_pf_religion_countries1_idx` (`number_id` ASC) VISIBLE,
  CONSTRAINT `fk_pf_religion_countries1`
    FOREIGN KEY (`number_id`)
    REFERENCES `mydb`.`countries` (`number_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`pf_association`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`pf_association` (
  `number_id` INT NOT NULL,
  `pf_association_association` DOUBLE NULL DEFAULT NULL,
  `pf_association_assembly` DOUBLE NULL DEFAULT NULL,
  `pf_association_political_establish` DOUBLE NULL DEFAULT NULL,
  `pf_association_political_operate` DOUBLE NULL DEFAULT NULL,
  `pf_association_political` DOUBLE NULL DEFAULT NULL,
  `pf_association_prof_establish` DOUBLE NULL DEFAULT NULL,
  `pf_association_prof_operate` DOUBLE NULL DEFAULT NULL,
  `pf_association_prof` DOUBLE NULL DEFAULT NULL,
  `pf_association_sport_establish` DOUBLE NULL DEFAULT NULL,
  `pf_association_sport_operate` DOUBLE NULL DEFAULT NULL,
  `pf_association_sport` DOUBLE NULL DEFAULT NULL,
  `pf_association` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`number_id`),
  INDEX `fk_pf_association_countries1_idx` (`number_id` ASC) VISIBLE,
  CONSTRAINT `fk_pf_association_countries1`
    FOREIGN KEY (`number_id`)
    REFERENCES `mydb`.`countries` (`number_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`pf_expression`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`pf_expression` (
  `number_id` INT NOT NULL,
  `pf_expression_killed` DOUBLE NULL DEFAULT NULL,
  `pf_expression_jailed` DOUBLE NULL DEFAULT NULL,
  `pf_expression_influence` DOUBLE NULL DEFAULT NULL,
  `pf_expression_control` DOUBLE NULL DEFAULT NULL,
  `pf_expression_cable` DOUBLE NULL DEFAULT NULL,
  `pf_expression_newspapers` DOUBLE NULL DEFAULT NULL,
  `pf_expression_internet` DOUBLE NULL DEFAULT NULL,
  `pf_expression` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`number_id`),
  INDEX `fk_pf_expression_countries1_idx` (`number_id` ASC) VISIBLE,
  CONSTRAINT `fk_pf_expression_countries1`
    FOREIGN KEY (`number_id`)
    REFERENCES `mydb`.`countries` (`number_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`pf_identity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`pf_identity` (
  `number_id` INT NOT NULL,
  `pf_identity_legal` DOUBLE NULL DEFAULT NULL,
  `pf_identity_parental_marriage` DOUBLE NULL DEFAULT NULL,
  `pf_identity_parental_divorce` DOUBLE NULL DEFAULT NULL,
  `pf_identity_parental` DOUBLE NULL DEFAULT NULL,
  `pf_identity_sex_male` DOUBLE NULL DEFAULT NULL,
  `pf_identity_sex_female` DOUBLE NULL DEFAULT NULL,
  `pf_identity_sex` DOUBLE NULL DEFAULT NULL,
  `pf_identity_divorce` DOUBLE NULL DEFAULT NULL,
  `pf_identity` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`number_id`),
  INDEX `fk_pf_identity_countries1_idx` (`number_id` ASC) VISIBLE,
  CONSTRAINT `fk_pf_identity_countries1`
    FOREIGN KEY (`number_id`)
    REFERENCES `mydb`.`countries` (`number_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`ef_government`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`ef_government` (
  `number_id` INT NOT NULL,
  `ef_government_consumption` DOUBLE NULL DEFAULT NULL,
  `ef_government_transfers` DOUBLE NULL DEFAULT NULL,
  `ef_government_enterprises` DOUBLE NULL DEFAULT NULL,
  `ef_government_tax_income` DOUBLE NULL DEFAULT NULL,
  `ef_government_tax_payroll` DOUBLE NULL DEFAULT NULL,
  `ef_government_tax` DOUBLE NULL DEFAULT NULL,
  `ef_government` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`number_id`),
  INDEX `fk_ef_government_countries1_idx` (`number_id` ASC) VISIBLE,
  CONSTRAINT `fk_ef_government_countries1`
    FOREIGN KEY (`number_id`)
    REFERENCES `mydb`.`countries` (`number_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`ef_legal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`ef_legal` (
  `number_id` INT NOT NULL,
  `ef_legal_judicial` DOUBLE NULL DEFAULT NULL,
  `ef_legal_courts` DOUBLE NULL DEFAULT NULL,
  `ef_legal_protection` DOUBLE NULL DEFAULT NULL,
  `ef_legal_military` DOUBLE NULL DEFAULT NULL,
  `ef_legal_integrity` DOUBLE NULL DEFAULT NULL,
  `ef_legal_enforcement` DOUBLE NULL DEFAULT NULL,
  `ef_legal_restrictions` DOUBLE NULL DEFAULT NULL,
  `ef_legal_police` DOUBLE NULL DEFAULT NULL,
  `ef_legal_crime` DOUBLE NULL DEFAULT NULL,
  `ef_legal_gender` DOUBLE NULL DEFAULT NULL,
  `ef_legal` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`number_id`),
  INDEX `fk_ef_legal_countries1_idx` (`number_id` ASC) VISIBLE,
  CONSTRAINT `fk_ef_legal_countries1`
    FOREIGN KEY (`number_id`)
    REFERENCES `mydb`.`countries` (`number_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`ef_money`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`ef_money` (
  `number_id` INT NOT NULL,
  `ef_money_growth` DOUBLE NULL DEFAULT NULL,
  `ef_money_sd` DOUBLE NULL DEFAULT NULL,
  `ef_money_inflation` DOUBLE NULL DEFAULT NULL,
  `ef_money_currency` DOUBLE NULL DEFAULT NULL,
  `ef_money` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`number_id`),
  INDEX `fk_ef_money_countries1_idx` (`number_id` ASC) VISIBLE,
  CONSTRAINT `fk_ef_money_countries1`
    FOREIGN KEY (`number_id`)
    REFERENCES `mydb`.`countries` (`number_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`ef_trade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`ef_trade` (
  `number_id` INT NOT NULL,
  `ef_trade_tariffs_revenue` INT NULL DEFAULT NULL,
  `ef_trade_tariffs_mean` DOUBLE NULL DEFAULT NULL,
  `ef_trade_tariffs_sd` DOUBLE NULL DEFAULT NULL,
  `ef_trade_tariffs` DOUBLE NULL DEFAULT NULL,
  `ef_trade_regulatory_nontariff` DOUBLE NULL DEFAULT NULL,
  `ef_trade_regulatory_compliance` DOUBLE NULL DEFAULT NULL,
  `ef_trade_regulatory` DOUBLE NULL DEFAULT NULL,
  `ef_trade_black` DOUBLE NULL DEFAULT NULL,
  `ef_trade_movement_foreign` DOUBLE NULL DEFAULT NULL,
  `ef_trade_movement_capital` DOUBLE NULL DEFAULT NULL,
  `ef_trade_movement_visit` DOUBLE NULL DEFAULT NULL,
  `ef_trade_movement` DOUBLE NULL DEFAULT NULL,
  `ef_trade` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`number_id`),
  INDEX `fk_ef_trade_countries1_idx` (`number_id` ASC) VISIBLE,
  CONSTRAINT `fk_ef_trade_countries1`
    FOREIGN KEY (`number_id`)
    REFERENCES `mydb`.`countries` (`number_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`ef_regulation_credit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`ef_regulation_credit` (
  `number_id` INT NOT NULL,
  `ef_regulation_credit_ownership` DOUBLE NULL DEFAULT NULL,
  `ef_regulation_credit_private` DOUBLE NULL DEFAULT NULL,
  `ef_regulation_credit_interest` DOUBLE NULL DEFAULT NULL,
  `ef_regulation_credit` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`number_id`),
  INDEX `fk_ef_regulation_credit_countries1_idx` (`number_id` ASC) VISIBLE,
  CONSTRAINT `fk_ef_regulation_credit_countries1`
    FOREIGN KEY (`number_id`)
    REFERENCES `mydb`.`countries` (`number_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`ef_regulation_labor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`ef_regulation_labor` (
  `number_id` INT NOT NULL,
  `ef_regulation_labor_minwage` DOUBLE NULL DEFAULT NULL,
  `ef_regulation_labor_firing` DOUBLE NULL DEFAULT NULL,
  `ef_regulation_labor_bargain` DOUBLE NULL DEFAULT NULL,
  `ef_regulation_labor_hours` DOUBLE NULL DEFAULT NULL,
  `ef_regulation_labor_dismissal` DOUBLE NULL DEFAULT NULL,
  `ef_regulation_labor_conscription` DOUBLE NULL DEFAULT NULL,
  `ef_regulation_labor` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`number_id`),
  INDEX `fk_ef_regulation_labor_countries1_idx` (`number_id` ASC) VISIBLE,
  CONSTRAINT `fk_ef_regulation_labor_countries1`
    FOREIGN KEY (`number_id`)
    REFERENCES `mydb`.`countries` (`number_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`ef_regulation_business`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`ef_regulation_business` (
  `number_id` INT NOT NULL,
  `ef_regulation_business_adm` DOUBLE NULL DEFAULT NULL,
  `ef_regulation_business_bureaucracy` DOUBLE NULL DEFAULT NULL,
  `ef_regulation_business_start` DOUBLE NULL DEFAULT NULL,
  `ef_regulation_business_bribes` DOUBLE NULL DEFAULT NULL,
  `ef_regulation_business_licensing` DOUBLE NULL DEFAULT NULL,
  `ef_regulation_business_compliance` DOUBLE NULL DEFAULT NULL,
  `ef_regulation_business` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`number_id`),
  INDEX `fk_ef_regulation_business_countries1_idx` (`number_id` ASC) VISIBLE,
  CONSTRAINT `fk_ef_regulation_business_countries1`
    FOREIGN KEY (`number_id`)
    REFERENCES `mydb`.`countries` (`number_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`rank_score`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`rank_score` (
  `number_id` INT NOT NULL,
  `pf_score` DOUBLE NULL DEFAULT NULL,
  `pf_rank` DOUBLE NULL DEFAULT NULL,
  `ef_score` DOUBLE NULL DEFAULT NULL,
  `ef_rank` DOUBLE NULL DEFAULT NULL,
  `hf_score` DOUBLE NULL DEFAULT NULL,
  `hf_rank` DOUBLE NULL DEFAULT NULL,
  `hf_quartile` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`number_id`),
  INDEX `fk_rank_score_countries1_idx` (`number_id` ASC) VISIBLE,
  CONSTRAINT `fk_rank_score_countries1`
    FOREIGN KEY (`number_id`)
    REFERENCES `mydb`.`countries` (`number_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`ef_regulation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`ef_regulation` (
  `number_id` INT NOT NULL,
  `ef_regulation` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`number_id`),
  CONSTRAINT `fk_ef_regulation_countries`
    FOREIGN KEY (`number_id`)
    REFERENCES `mydb`.`countries` (`number_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `mydb`.`countries`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`countries` (`number_id`, `year`, `ISO_code`, `countries`, `region`) VALUES (1, 2016, 'ALB', 'Albania', 'Eastern Europe');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`pf_rol`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`pf_rol` (`number_id`, `pf_rol_procedural`, `pf_rol_civil`, `pf_rol_criminal`, `pf_rol`) VALUES (1, 6.661502941, 4.547243777, 4.666508223, 5.291751647);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`pf_ss`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`pf_ss` (`number_id`, `pf_ss_homicide`, `pf_ss_disappearances_disap`, `pf_ss_disappearances_violent`, `pf_ss_disappearances_organized`, `pf_ss_disappearances_fatalities`, `pf_ss_disappearances_injuries`, `pf_ss_disappearances`, `pf_ss_women_fgm`, `pf_ss_women_missing`, `pf_ss_women_inheritance_widows`, `pf_ss_women_inheritance_daughters`, `pf_ss_women_inheritance`, `pf_ss_women`, `pf_ss`) VALUES (1, 8.920429431, 10, 10, 10, 10, 10, 10, 10, 7.5, 5, 5, 5, 7.5, 8.80680981);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`pf_movement`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`pf_movement` (`number_id`, `pf_movement_domestic`, `pf_movement_foreign`, `pf_movement_women`, `pf_movement`) VALUES (1, 5, 10, 5, 6.666666667);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`pf_religion`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`pf_religion` (`number_id`, `pf_religion_estop_establish`, `pf_religion_estop_operate`, `pf_religion_estop`, `pf_religion_harassment`, `pf_religion_restrictions`, `pf_religion`) VALUES (1, NULL, NULL, 10, 9.566666667, 8.011111111, 9.192592593);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`pf_association`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`pf_association` (`number_id`, `pf_association_association`, `pf_association_assembly`, `pf_association_political_establish`, `pf_association_political_operate`, `pf_association_political`, `pf_association_prof_establish`, `pf_association_prof_operate`, `pf_association_prof`, `pf_association_sport_establish`, `pf_association_sport_operate`, `pf_association_sport`, `pf_association`) VALUES (1, 10, 10, NULL, NULL, 10, NULL, NULL, 10, NULL, NULL, 10, 10);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`pf_expression`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`pf_expression` (`number_id`, `pf_expression_killed`, `pf_expression_jailed`, `pf_expression_influence`, `pf_expression_control`, `pf_expression_cable`, `pf_expression_newspapers`, `pf_expression_internet`, `pf_expression`) VALUES (1, 10, 10, 5, 5.25, 10, 10, 10, 8.607142857);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`pf_identity`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`pf_identity` (`number_id`, `pf_identity_legal`, `pf_identity_parental_marriage`, `pf_identity_parental_divorce`, `pf_identity_parental`, `pf_identity_sex_male`, `pf_identity_sex_female`, `pf_identity_sex`, `pf_identity_divorce`, `pf_identity`) VALUES (1, 0, 10, 10, 10, 10, 10, 10, 5, 6.25);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`ef_government`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`ef_government` (`number_id`, `ef_government_consumption`, `ef_government_transfers`, `ef_government_enterprises`, `ef_government_tax_income`, `ef_government_tax_payroll`, `ef_government_tax`, `ef_government`) VALUES (1, 8.232352941, 7.509902296, 8, 9, 7, 8, 7.935563809);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`ef_legal`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`ef_legal` (`number_id`, `ef_legal_judicial`, `ef_legal_courts`, `ef_legal_protection`, `ef_legal_military`, `ef_legal_integrity`, `ef_legal_enforcement`, `ef_legal_restrictions`, `ef_legal_police`, `ef_legal_crime`, `ef_legal_gender`, `ef_legal`) VALUES (1, 2.668221792, 3.145461678, 4.512227773, 8.333333333, 4.166666667, 4.387444055, 6.48528661, 6.933499972, 6.215400695, 0.948717949, 5.071813726);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`ef_money`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`ef_money` (`number_id`, `ef_money_growth`, `ef_money_sd`, `ef_money_inflation`, `ef_money_currency`, `ef_money`) VALUES (1, 8.986454287, 9.484575272, 9.7436, 10, 9.55365739);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`ef_trade`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`ef_trade` (`number_id`, `ef_trade_tariffs_revenue`, `ef_trade_tariffs_mean`, `ef_trade_tariffs_sd`, `ef_trade_tariffs`, `ef_trade_regulatory_nontariff`, `ef_trade_regulatory_compliance`, `ef_trade_regulatory`, `ef_trade_black`, `ef_trade_movement_foreign`, `ef_trade_movement_capital`, `ef_trade_movement_visit`, `ef_trade_movement`, `ef_trade`) VALUES (1, 9.626666667, 9.24, 8.024, 8.963555556, 5.574481487, 9.405327776, 7.489904631, 10, 6.306105852, 4.615384615, 8.296923099, 6.406137855, 8.21489951);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`ef_regulation_credit`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`ef_regulation_credit` (`number_id`, `ef_regulation_credit_ownership`, `ef_regulation_credit_private`, `ef_regulation_credit_interest`, `ef_regulation_credit`) VALUES (1, 5, 7.295686921, 9, 7.098562307);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`ef_regulation_labor`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`ef_regulation_labor` (`number_id`, `ef_regulation_labor_minwage`, `ef_regulation_labor_firing`, `ef_regulation_labor_bargain`, `ef_regulation_labor_hours`, `ef_regulation_labor_dismissal`, `ef_regulation_labor_conscription`, `ef_regulation_labor`) VALUES (1, 5.566666667, 5.396398703, 6.234861215, 8, 6.299740913, 10, 6.916277916);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`ef_regulation_business`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`ef_regulation_business` (`number_id`, `ef_regulation_business_adm`, `ef_regulation_business_bureaucracy`, `ef_regulation_business_start`, `ef_regulation_business_bribes`, `ef_regulation_business_licensing`, `ef_regulation_business_compliance`, `ef_regulation_business`) VALUES (1, 6.072171528, 6, 9.713863752, 4.050195972, 7.324582113, 7.074365792, 6.705863193);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`rank_score`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`rank_score` (`number_id`, `pf_score`, `pf_rank`, `ef_score`, `ef_rank`, `hf_score`, `hf_rank`, `hf_quartile`) VALUES (1, 7.596280576, 57, 7.54, 34, 7.568140288, 48, 2);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`ef_regulation`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`ef_regulation` (`number_id`, `ef_regulation`) VALUES (1, 6.906901139);

COMMIT;

